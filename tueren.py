knoepfe = {
    'A': [0b1000, 0b0100, 0b0010, 0b0001],
    'B': [0b1100, 0b0110, 0b0011, 0b1001],
    'C': [0b1010, 0b0101]
}

class Druecker:

    def __init__(self, folge):
        self.folge = folge

    def druecke(self, riegel, knopf):
        if riegel == 0b0000 or riegel == 0b1111:
            return 0
        elif knopf >= len(self.folge):
            return 1
        else:
            fehler = 0
            for schieben in knoepfe[self.folge[knopf]]:
                fehler += self.druecke(riegel ^ schieben, knopf + 1)
            return fehler

    def test(self):
        fehler = []
        for riegel in range(0b0000, 0b1111 + 1):
            if self.druecke(riegel, 0) != 0:
                fehler.append(riegel)
        return fehler

def test(folge, laenge):
    if laenge:
        for name in knoepfe.keys():
            test(folge + name, laenge - 1)
    else:
        druecker = Druecker(folge)
        fehler = druecker.test()
        if fehler:
            pass
            # print(folge, 'Fehler:', ' '.join(f'{riegel:04b}' for riegel in fehler))
        else:
            print(folge, 'OK')

for laenge in range(0, 8):
    test('', laenge)
